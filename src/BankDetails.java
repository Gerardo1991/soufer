/**
 * 
 */


/**
 * These are concrete prototypes for clone
 * @author Idiotechie
 * 
 */
public class BankDetails extends UserProfile {
	int bankAccount;

	public int getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(int bankAccount) {
		this.bankAccount = bankAccount;
	}
}
